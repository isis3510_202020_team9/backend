# Backend



## Folder structure
```sh
├── tsconfig.json
├── package.json
├── ormconfig.ts
├── tsconfig.json
├── config.ts
└── .gitignore
└── src
    ├── entity/
    ├── middlewares/
    ├── migration/
    ├── models/
    ├── routes/
    ├── utils/
        ├── db/
index.ts
```



## Run a migration based on dev

```
npx ts-node ./node_modules/typeorm/cli -f build/src/utils/db/migrations.config.js migration:generate -n firstMigration
```