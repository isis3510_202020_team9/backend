import "reflect-metadata";
import express from "express";
import cors from "cors";
import './config';
import userRouter from "./src/routes/user-routes";
import ventureRouter from "./src/routes/venture-routes";
import productRouter from "./src/routes/product-routes";



const app = express();

app.use(express.json());
app.use(cors());
app.use(express.urlencoded({ extended: false }));
// Endpoints 
app.use('/users', userRouter);
app.use('/ventures', ventureRouter);
app.use('/products', productRouter);

app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};
  // render the error page
  res.status(err.status || 500).send({err});
});
app.listen(process.env.PORT || 5000);

