import { PostgresConnectionOptions } from "typeorm/driver/postgres/PostgresConnectionOptions";
import { SnakeNamingStrategy } from "typeorm-naming-strategies";
import "./config";

const getConfig = (databaseName: string): PostgresConnectionOptions => {
  return {
    type: "postgres",
    name: databaseName,
    database: process.env.DB_NAME,
    port: parseInt(process.env.DB_PORT, 10) || 5432,
    synchronize: false, // Use true for local development
    migrationsRun: process.env.ENVIROMENT === "prod",
    namingStrategy: new SnakeNamingStrategy(),
    logging: process.env.ENVIROMENT === "prod",
    host: process.env.DB_HOST || "0.0.0.0",
    username: process.env.DB_USERNAME || "postgres",
    password: process.env.DB_PASSWORD || "postgres",
    entities: [process.env.ENTITY_PATH] || ["src/entity/*.ts"],
    migrations: [process.env.MIGRATIONS_PATH] || ["src/migration/*.ts"],
    cli: {
      entitiesDir: "src/entity",
      migrationsDir: "src/migration",
      subscribersDir: "src/subscriber",
    },
    ssl: true,
  };
};
export default getConfig;
