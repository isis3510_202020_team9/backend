import rp from 'request-promise';


/**
 * Coordinate interface
 */
export interface Coordinate {
    latitude: number;
    longitude: number;
}

/**
 * Get distance between two points
 * @param coordinates - Coordinates 
 * @param possibleNearDirection - Coordinates
 */
export const getDistance = (
    coordinates: Coordinate,
    possibleNearDirection: Coordinate
) => {
    const radlat1 = (Math.PI * coordinates.latitude) / 180;
    const longitudeCenter = coordinates.longitude;
    const { latitude, longitude } = possibleNearDirection;
    const radlat2 = (Math.PI * latitude) / 180;
    const theta = longitudeCenter - longitude;
    const radtheta = (Math.PI * theta) / 180;
    let dist =
        Math.sin(radlat1) * Math.sin(radlat2) +
        Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    if (dist > 1) {
        dist = 1;
    }
    dist = Math.acos(dist);
    dist = (dist * 180) / Math.PI;
    dist = dist * 60 * 1.1515;
    dist = dist * 1.609344;
    return dist;
};

export interface PositionStackResponse {
    data: Geolocation[];
}

interface Geolocation {
    latitude: number;
    longitude: number;
    label: string;
    name: string;
    type: string;
    number: string;
    street: string;
    postal_code: string;
    confidence: number;
    region: string;
    region_code: string;
    administrative_area: null;
    neighbourhood: string;
    country: string;
    country_code: string;
    map_url: string;
}

/**
 * Forward geocoding
 * Address -> LatLong
 * @param address - Address
 */
export const forwardGeocoding = async (address: string) => {
    const apiKey = process.env.POSITION_STACK_API_KEY;
    const uri = `http://api.positionstack.com/v1/forward?access_key=${apiKey}&country=CO&region=Bogota&limit=1&query=${address}`;
    const response: PositionStackResponse = JSON.parse(await rp(uri).promise());
    const { latitude, longitude } = response?.data[0] && response?.data?.length > 0 ? response.data[0] : { latitude: 4.757721, longitude: -74.067366};
    return { latitude, longitude };
};