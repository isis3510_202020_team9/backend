import jwt from "jsonwebtoken";
import bcrypt from 'bcrypt';
import '../../../config'

/**
 * Verify if user could use the middleware
 * @param req
 * @param res
 */
export const checkToken = async (req, res) => {
  let finished = false;
  try {
    res = await verifyToken(req, res);
    if (res.locals.token) finished = true;
  } catch (error) {
    console.error(error);
  }
  if (finished) return res;
  else
    return res.status(401).json({
      success: false,
      message: "The token is not valid",
    });
};

/**
 * Verify token 
 * @param req
 * @param res
 * @param userGroup
 */
const verifyToken = async (req, res) => {
  const key = process.env.SECRET_KEY;
  // Extrae el token de la solicitud enviado a través de cualquiera de los dos headers especificados
  // Los headers son automáticamente convertidos a lowercase
  let token = req.headers["x-access-token"] || req.headers["authorization"];

  if (token) {
    if (token.startsWith("Bearer ")) {
      token = token.slice(7, token.length);
      // Llama la función verify del paquete jsonwebtoken que se encarga de realizar la validación del token con el secret proporcionado
      jwt.verify(token, key, (err, decoded) => {
        // Si no pasa la validación, un mensaje de error es retornado
        // de lo contrario, permite a la solicitud continuar
        if (!err) {
          req.decoded = decoded;
          res.locals.token = token;
        } else res.locals.token = null;
      });
    } else {
      try {
        const decode = jwt.verify(token, key);
        res.decode = decode;
        res.locals.token = token;
        return res;
      } catch (error) {
        throw "Auth process fails. You are not allowed to access to this service";
      }
    }
  } else {
    throw "Auth token is not supplied";
  }
};
/**
 * Generate new token
 * @param id - Client id
 */
export const generateToken = (id: string) => {
  const key = process.env.SECRET_KEY;
  try {
    return jwt.sign({ id }, key);
  } catch (error) {
    throw error;
  }
};
/**
 * Hash password
 * @param password 
 */
export const hashPassword = async (password: string) => {
  const salt = await bcrypt.genSalt(16);
  return await bcrypt.hash(password, salt);
};
/**
 * Verify password
 * @param password - Password entered 
 * @param actualPassword - Actual password
 */
export const verifyPassword = async (
  password: string,
  actualPassword: string,
) => {
    return await bcrypt.compare(password,actualPassword);
};
