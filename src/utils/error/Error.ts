
export enum CreaturCode {
  AUTH = '01'
}
/**
 * Error interface
 */
export interface CreaturError {
    errorCode: number;
    creaturCode: CreaturCode;
    message: string;
  }

/**
 * Return an error
 * @param code
 * @param message
 */
export const generateError = (message: string, creaturCode?: CreaturCode, httpCode?: number) => {
  // For default we use 400
  if (!httpCode) httpCode = 400; 
  const error: CreaturError = { errorCode: httpCode, creaturCode, message };
  return error;
};