import { EntityManager, Connection } from "typeorm";
import { User, UserRole } from "../../entity/User";
import { checkToken } from "../auth/jwt";
import { Database } from "../db/Database";
import { CreaturCode, CreaturError, generateError } from "../error/Error";

/**
 * Middleware context
 */
export interface Context {
  manager: EntityManager;
  userId?: string;
}

/**
 * Middleware class
 * Wraps request and response
 */
export class Middleware<M> {
  /** Request event */
  event: any;
  /** Response event */
  responder: any;
  /** Possible user roles */
  userRoles?: UserRole[];
  /** Disable jwt validation */
  disableAuth?: boolean;

  /** DB Connection */
  private connection: Connection;
  /**
   * Handler
   */
  handler: (
    event: any,
    manager: Context
  ) => Promise<M | CreaturError> | void | CreaturError;

  /**
   * Build a new middleware
   * @param event
   * @param responder
   * @param handler
   * @param auth
   * @param userRoles
   */
  constructor(payload: {
    event: any;
    responder: any;
    handler: (
      event: any,
      context: Context
    ) => Promise<M | CreaturError> | void | CreaturError;
    userRoles?: UserRole[];
    auth?: boolean;
  }) {
    this.event = payload.event;
    this.handler = payload.handler;
    this.responder = payload.responder;
    this.userRoles = payload.userRoles;
    this.disableAuth = payload.auth;
  }

  /**
   * Execute middleware
   */
  private async execute() {
    try {
      const used = process.memoryUsage().heapUsed / 1024 / 1024;
      console.log('Heap used at the beginning of the request', used);
  
      this.connection = await new Database().getConnection();
      const { manager } = this.connection;
      const context: Context = { manager };

      // check if token is valid
      if (this.disableAuth) {
        console.log('Starting token validation...');
        this.responder = await checkToken(this.event, this.responder);
        const userId = this.responder.decode ? this.responder.decode.id : null;
        const user = await manager.findOne(User, userId);
        if (!user || !this.userRoles.includes(user.role))
          return generateError(
            "You cannot access to this function",
            CreaturCode.AUTH,
            401
          );
        else context.userId = userId;
        return await this.handler(this.event, context);
      } else return await this.handler(this.event, context);
    } catch (error) {
      console.log(`Request failed... ${JSON.stringify(error)}`);
      throw error;
    }
    finally {
      const used = process.memoryUsage().heapUsed / 1024 / 1024;
      console.log('Heap used at the end of the request', used);
    }
  }
  /**
   * Response wrapped and handle errors
   */
  async responseWrapped() {
    try {
      const response = await this.execute();
      // Send app handled error
      if ((response as CreaturError)?.errorCode) {
        const error = response as CreaturError;
        this.responder.status(error.errorCode).send({ error: error.message });
        return;
      }
      if (this.responder) this.responder.send(response);
    } catch (error) {
      if ((error as CreaturError)?.errorCode) {
        this.responder.status(error.errorCode).send({ error: error.message });
        return;
      }
      if (this.responder.statusCode === 401) return;
      this.responder.status(500).send({ error: error.toString() });
    } finally {
      console.log("Finishing connection ...");
    }
  }
}
