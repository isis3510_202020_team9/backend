import { Connection, ConnectionManager, FindManyOptions, getConnectionManager, PrimaryGeneratedColumn, Repository } from "typeorm";
import getConfig from "../../../ormconfig";
import { PostgresConnectionOptions } from "typeorm/driver/postgres/PostgresConnectionOptions";
import '../../../config';
import { flatten, groupBy, map, merge } from "lodash";
export class Database {
    private config: PostgresConnectionOptions;
    private connectionManager: ConnectionManager;

    constructor() {
        const databaseName = process.env.DB_NAME;
        this.config = getConfig(databaseName);
    }
    private injectConnection(connectionName: string) {
        const connection = this.connectionManager.get(connectionName);
        // @ts-ignore
        connection.options = this.config;
        // @ts-ignore
        connection.manager = connection.createEntityManager();
        // @ts-ignore
        connection.namingStrategy = connection.options.namingStrategy;

        // @ts-ignore
        if (!connection.relationLoader) connection.relationLoader = new RelationLoader(connection);

        // @ts-ignore
        if (!connection.relationIdLoader) connection.relationIdLoader = new RelationIdLoader(connection);
        
        // @ts-ignore
        connection.buildMetadatas();

        return connection;
    }
    public async getConnection() : Promise<Connection> {
        try {
        this.connectionManager = getConnectionManager();
            let connection: Connection;
            if (
                this.connectionManager.has(this.config.name) &&
                this.connectionManager.get(this.config.name).isConnected
            ) {
                connection = this.injectConnection(this.config.name);
            }
            else {
                connection = this.connectionManager.create(this.config);
                await connection.connect();
            }
            console.log('Connection established');
            return connection;
        } catch (e) {
            console.error(e);
            throw e;
        }
    }
}

// Repository for heavy queries 
export class GenericRepository<E extends IdentifiableEntity>  {
    repository: Repository<E>;
    constructor(repository: Repository<E>) {
        this.repository = repository;
    }
    public async findWithRelations(
      relations: Array<keyof E> = [],
      optionsWithoutRelations: Omit<FindManyOptions<E>, 'relations'> = {}
    ): Promise<E[]> {
      const entities = await this.repository.find(optionsWithoutRelations);
      const entitiesIds = entities.map(({ id }) => id);
      const entitiesIdsWithRelations = await Promise.all(
        relations.map((relation) => this.repository.findByIds(entitiesIds, { select: ['id'], relations: [relation as string] }))
      ).then(flatten);
      const entitiesAndRelations = entitiesIdsWithRelations.concat(entities);
  
      const entitiesAndRelationsById = groupBy(entitiesAndRelations, 'id')
      return map(entitiesAndRelationsById, (entityAndRelations) => merge({}, ...entityAndRelations))
    }

    public async findRelations(
        entities: E[],
        relations: Array<keyof E> = [],
    ): Promise<E[]> {
        const entitiesIds = entities.map(({ id }) => id);
        const entitiesIdsWithRelations = await Promise.all(
          relations.map((relation) => this.repository.findByIds(entitiesIds, { select: ['id'], relations: [relation as string] }))
        ).then(flatten);
        const entitiesAndRelations = entitiesIdsWithRelations.concat(entities);
    
        const entitiesAndRelationsById = groupBy(entitiesAndRelations, 'id')
        return map(entitiesAndRelationsById, (entityAndRelations) => merge({}, ...entityAndRelations))  
    }
  }
  // Abstract class that represents an typeorm entity
  export abstract class IdentifiableEntity {
    @PrimaryGeneratedColumn()
    public id: string;
  }  
  