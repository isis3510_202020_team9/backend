import { ConnectionOptions } from "typeorm";
import { SnakeNamingStrategy } from "typeorm-naming-strategies";

const config: ConnectionOptions = {
    "type": "postgres",
    "host": process.env.DB_HOST,
    "port": parseInt(process.env.DB_PORT,10),
    "username": process.env.DB_USERNAME,
    "password": process.env.DB_PASSWORD,
    "database": process.env.DB_NAME,
    "extra" : { 
       "ssl" : true,
    },
    "entities": ['build/src/entity/*.js'],
    "migrations": [
       process.env.MIGRATIONS_PATH
    ],
    "subscribers": [
       "src/subscriber/**/*.js"
    ],
    "cli": {
       "entitiesDir": "src/entity",
       "migrationsDir": "src/migration",
       "subscribersDir": "build/subscriber"
    },
    "namingStrategy": new SnakeNamingStrategy(),

}

export = config;