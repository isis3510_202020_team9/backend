import { Category } from "../entity/VentureCategory";

/// Questions
export const questions = [
    {
        "title": "¿Qué prefieres hacer los fines de semana?",
        "id": 1,
        "answers": [
            {
                "id": 1,
                "title": "Ir a comprar ropa nueva",
                "weights": [
                    {
                        "category": Category.CLOTHING,
                        "weight": 10
                    },
                    {
                        "category": Category.FAST_FOOD,
                        "weight": 3
                    },
                    {
                        "category": Category.PETS,
                        "weight": 1
                    },
                    {
                        "category": Category.TOYS,
                        "weight": 1
                    },
                    {
                        "category": Category.BAKERY,
                        "weight": 3
                    }, {
                        "category": Category.SPORTS,
                        "weight": 1
                    },
                ]
            },
            {
                "id": 2,
                "title": "Salir a comer con mis amigos.",
                "weights": [
                    {
                        "category": Category.CLOTHING,
                        "weight": 5
                    },
                    {
                        "category": Category.FAST_FOOD,
                        "weight": 10
                    },
                    {
                        "category": Category.PETS,
                        "weight": 1
                    },
                    {
                        "category": Category.TOYS,
                        "weight": 1
                    },
                    {
                        "category": Category.BAKERY,
                        "weight": 9
                    },
                    {
                        "category": Category.SPORTS,
                        "weight": 6
                    },
                ]
            },
            {
                "id": 3,
                "title": "Ir al zoológico",
                "weights": [
                    {
                        "category": Category.CLOTHING,
                        "weight": 2
                    },
                    {
                        "category": Category.FAST_FOOD,
                        "weight": 2
                    },
                    {
                        "category": Category.PETS,
                        "weight": 8
                    },
                    {
                        "category": Category.TOYS,
                        "weight": 2
                    },
                    {
                        "category": Category.BAKERY,
                        "weight": 2
                    },
                    {
                        "category": Category.SPORTS,
                        "weight": 5
                    },
                ]
            },
            {
                "id": 4,
                "title": "Ver netflix mientras como helado.",
                "weights": [
                    {
                        "category": Category.CLOTHING,
                        "weight": 1
                    },
                    {
                        "category": Category.FAST_FOOD,
                        "weight": 8
                    },
                    {
                        "category": Category.PETS,
                        "weight": 2
                    },
                    {
                        "category": Category.TOYS,
                        "weight": 1
                    },
                    {
                        "category": Category.BAKERY,
                        "weight": 10
                    },
                    {
                        "category": Category.SPORTS,
                        "weight": 3
                    },
                ]
            },
            {
                "id": 5,
                "title": "Tener una tarde llena de juegos de mesa.",
                "weights":
                    [
                        {
                            "category": Category.CLOTHING,
                            "weight": 2
                        },
                        {
                            "category": Category.FAST_FOOD,
                            "weight": 3
                        },
                        {
                            "category": Category.PETS,
                            "weight": 2
                        },
                        {
                            "category": Category.TOYS,
                            "weight": 10
                        },
                        {
                            "category": Category.BAKERY,
                            "weight": 3
                        },
                        {
                            "category": Category.SPORTS,
                            "weight": 1
                        },
                    ]
            },
            {
                "id": 6,
                "title": "Quedarme en casa viendo partidos.",
                "weights": [
                    {
                        "category": Category.CLOTHING,
                        "weight": 3
                    },
                    {
                        "category": Category.FAST_FOOD,
                        "weight": 8
                    },
                    {
                        "category": Category.PETS,
                        "weight": 3
                    },
                    {
                        "category": Category.TOYS,
                        "weight": 1
                    },
                    {
                        "category": Category.BAKERY,
                        "weight": 6
                    },
                    {
                        "category": Category.SPORTS,
                        "weight": 10
                    },
                ]
            },
        ]
    },
    {
        "title": "¿Qué aplicación descargarías primero?",
        "id": 2,
        "answers": [
            {
                "id": 1,
                "title": "Twitch",
                "weights": [
                    {
                        "category": Category.CLOTHING,
                        "weight": 1
                    },
                    {
                        "category": Category.FAST_FOOD,
                        "weight": 1
                    },
                    {
                        "category": Category.PETS,
                        "weight": 1
                    },
                    {
                        "category": Category.TOYS,
                        "weight": 10
                    },
                    {
                        "category": Category.BAKERY,
                        "weight": 1
                    }, {
                        "category": Category.SPORTS,
                        "weight": 7
                    },
                ]
            },
            {
                "id": 2,
                "title": "Rappi",
                "weights": [
                    {
                        "category": Category.CLOTHING,
                        "weight": 4
                    },
                    {
                        "category": Category.FAST_FOOD,
                        "weight": 10
                    },
                    {
                        "category": Category.PETS,
                        "weight": 4
                    },
                    {
                        "category": Category.TOYS,
                        "weight": 3
                    },
                    {
                        "category": Category.BAKERY,
                        "weight": 9
                    },
                    {
                        "category": Category.SPORTS,
                        "weight": 1
                    },
                ]
            },
            {
                "id": 3,
                "title": "Nike training, Onefootball, Google Now",
                "weights": [
                    {
                        "category": Category.CLOTHING,
                        "weight": 4
                    },
                    {
                        "category": Category.FAST_FOOD,
                        "weight": 1
                    },
                    {
                        "category": Category.PETS,
                        "weight": 1
                    },
                    {
                        "category": Category.TOYS,
                        "weight": 2
                    },
                    {
                        "category": Category.BAKERY,
                        "weight": 1
                    },
                    {
                        "category": Category.SPORTS,
                        "weight": 10
                    },
                ]
            },
            {
                "id": 4,
                "title": "Laika",
                "weights": [
                    {
                        "category": Category.CLOTHING,
                        "weight": 1
                    },
                    {
                        "category": Category.FAST_FOOD,
                        "weight": 1
                    },
                    {
                        "category": Category.PETS,
                        "weight": 10
                    },
                    {
                        "category": Category.TOYS,
                        "weight": 2
                    },
                    {
                        "category": Category.BAKERY,
                        "weight": 1
                    },
                    {
                        "category": Category.SPORTS,
                        "weight": 1
                    },
                ]
            },
            {
                "id": 5,
                "title": "Trendier",
                "weights":
                    [
                        {
                            "category": Category.CLOTHING,
                            "weight": 10
                        },
                        {
                            "category": Category.FAST_FOOD,
                            "weight": 1
                        },
                        {
                            "category": Category.PETS,
                            "weight": 1
                        },
                        {
                            "category": Category.TOYS,
                            "weight": 1
                        },
                        {
                            "category": Category.BAKERY,
                            "weight": 1
                        },
                        {
                            "category": Category.SPORTS,
                            "weight": 1
                        },
                    ]
            },
        ]
    },
    {
        "title" : "¿Qué harías primero si te ganaras la lotería?",
        "id" : 3,
        "answers" : [
            {
                "id": 1,
                "title": "Viajar por el mundo probando diferentes gastronomías.",
                "weights": [
                    {
                        "category": Category.CLOTHING,
                        "weight": 4
                    },
                    {
                        "category": Category.FAST_FOOD,
                        "weight": 10
                    },
                    {
                        "category": Category.PETS,
                        "weight": 1
                    },
                    {
                        "category": Category.TOYS,
                        "weight": 1
                    },
                    {
                        "category": Category.BAKERY,
                        "weight": 10
                    }, {
                        "category": Category.SPORTS,
                        "weight": 1
                    },
                ]
            },
            {
                "id": 2,
                "title": "Gastarme todo el dinero en joyas y ropa.",
                "weights": [
                    {
                        "category": Category.CLOTHING,
                        "weight": 10
                    },
                    {
                        "category": Category.FAST_FOOD,
                        "weight": 1
                    },
                    {
                        "category": Category.PETS,
                        "weight": 1
                    },
                    {
                        "category": Category.TOYS,
                        "weight": 1
                    },
                    {
                        "category": Category.BAKERY,
                        "weight": 1
                    },
                    {
                        "category": Category.SPORTS,
                        "weight": 1
                    },
                ]
            },
            {
                "id": 3,
                "title": "Regalar juguetes a fundaciones.",
                "weights": [
                    {
                        "category": Category.CLOTHING,
                        "weight": 2
                    },
                    {
                        "category": Category.FAST_FOOD,
                        "weight": 1
                    },
                    {
                        "category": Category.PETS,
                        "weight": 3
                    },
                    {
                        "category": Category.TOYS,
                        "weight": 7
                    },
                    {
                        "category": Category.BAKERY,
                        "weight": 2
                    },
                    {
                        "category": Category.SPORTS,
                        "weight": 2
                    },
                ]
            },
            {
                "id": 4,
                "title": "Ir a los próximos Juegos Olímpicos.",
                "weights": [
                    {
                        "category": Category.CLOTHING,
                        "weight": 1
                    },
                    {
                        "category": Category.FAST_FOOD,
                        "weight": 1
                    },
                    {
                        "category": Category.PETS,
                        "weight": 1
                    },
                    {
                        "category": Category.TOYS,
                        "weight": 2
                    },
                    {
                        "category": Category.BAKERY,
                        "weight": 1
                    },
                    {
                        "category": Category.SPORTS,
                        "weight": 10
                    },
                ]
            },
            {
                "id": 5,
                "title": "Comprarme una casa enorme donde pueda tener las mascotas posibles.",
                "weights":
                    [
                        {
                            "category": Category.CLOTHING,
                            "weight": 1
                        },
                        {
                            "category": Category.FAST_FOOD,
                            "weight": 1
                        },
                        {
                            "category": Category.PETS,
                            "weight": 10
                        },
                        {
                            "category": Category.TOYS,
                            "weight": 4
                        },
                        {
                            "category": Category.BAKERY,
                            "weight": 1
                        },
                        {
                            "category": Category.SPORTS,
                            "weight": 1
                        },
                    ]
            },
        ]
 
    }
]