import express from "express";
import { UserRole } from "../entity/User";
import { createProduct, convertIntoVenture, getNearVentures, getSuggestedVentures, getTrendyVentures, getVentureOrders, getProducts, updateOrderStatus } from "../middlewares/venture-middleware";
import { Middleware } from "../utils/middleware/Middleware";

/**
 * Venture routes 
 */
enum VentureRoutes {
  SUGGESTED = '/suggested',
  NEAR = '/near',
  TRENDY = '/trendy',
  ORDERS = '/orders',
  PRODUCTS = '/products',
  PRODUCTS_LIST = '/:ventureId/products',
  START = '/start',
  ORDERS_STATUS = '/orders/:orderId/status'
}

/** Venture router */
const ventureRouter = express.Router();

// GET ventures/suggested
ventureRouter.get(
  VentureRoutes.SUGGESTED,
  async (req, res) =>
    await new Middleware({
      event: req,
      responder: res,
      handler: (event, context) => getSuggestedVentures(event, context.userId, context.manager),
      auth: true,
      userRoles: [UserRole.CLIENT, UserRole.VENTURE],
    }).responseWrapped()
);

// GET ventures/near
ventureRouter.get(
  VentureRoutes.NEAR,
  async (req, res) =>
    await new Middleware({
      event: req,
      responder: res,
      handler: (event, context) => getNearVentures(event, context.userId, context.manager),
      auth: true,
      userRoles: [UserRole.CLIENT, UserRole.VENTURE],
    }).responseWrapped()
);


ventureRouter.get(
  VentureRoutes.TRENDY,
  async (req, res) =>
    await new Middleware({
      event: req,
      responder: res,
      handler: (event, context) => getTrendyVentures(context.manager),
      auth: true,
      userRoles: [UserRole.CLIENT, UserRole.VENTURE],
    }).responseWrapped()
);


ventureRouter.get(
  VentureRoutes.ORDERS,
  async (req, res) =>
    await new Middleware({
      event: req,
      responder: res,
      handler: (event, context) => getVentureOrders(context.userId, context.manager),
      auth: true,
      userRoles: [UserRole.CLIENT, UserRole.VENTURE],
    }).responseWrapped()
);

ventureRouter.post(
  VentureRoutes.START,
  async (req, res) =>
    await new Middleware({
      event: req,
      responder: res,
      handler: (event, context) => convertIntoVenture(event, context.userId, context.manager),
      auth: true,
      userRoles: [UserRole.CLIENT],
    }).responseWrapped()
);

ventureRouter.post(
  VentureRoutes.PRODUCTS,
  async (req, res) =>
    await new Middleware({
      event: req,
      responder: res,
      handler: (event, context) => createProduct(event.body, context.userId, context.manager),
      auth: true,
      userRoles: [UserRole.VENTURE],
    }).responseWrapped()
);

ventureRouter.get(
  VentureRoutes.PRODUCTS_LIST,
  async (req, res) =>
    await new Middleware({
      event: req,
      responder: res,
      handler: (event, context) => getProducts(event, context.manager),
      auth: true,
      userRoles: [UserRole.CLIENT, UserRole.VENTURE],
    }).responseWrapped()
);

ventureRouter.put(
  VentureRoutes.ORDERS_STATUS,
  async (req, res) =>
    await new Middleware({
      event: req,
      responder: res,
      handler: (event, context) => updateOrderStatus(event, context.userId, context.manager),
      auth: true,
      userRoles: [UserRole.VENTURE],
    }).responseWrapped()
);

export default ventureRouter;