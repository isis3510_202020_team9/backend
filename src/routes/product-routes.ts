import express from "express";
import { UserRole } from "../entity/User";
import { buyProducts, getSuggestedProducts, getTrendyProducts } from "../middlewares/product-middleware";
import { Middleware } from "../utils/middleware/Middleware";


/**
 * Venture routes 
 */
enum ProductRoutes {
    SUGGESTED = '/suggested',
    TRENDY = '/trendy',
    BUY = '/buy',
}

const productRouter = express.Router();

// GET products/suggested
productRouter.get(
    ProductRoutes.SUGGESTED,
    async (req, res) =>
    await new Middleware({
      event: req,
      responder: res,
      handler: (event, context) => getSuggestedProducts(event, context.userId, context.manager),
      auth: true,
      userRoles: [UserRole.CLIENT, UserRole.VENTURE],
    }).responseWrapped()
);

// GET products/suggested
productRouter.get(
  ProductRoutes.TRENDY,
  async (req, res) =>
  await new Middleware({
    event: req,
    responder: res,
    handler: (event, context) => getTrendyProducts(event, context.manager),
    auth: true,
    userRoles: [UserRole.CLIENT, UserRole.VENTURE],
  }).responseWrapped()
);



productRouter.post(
  ProductRoutes.BUY,
  async (req, res) =>
  await new Middleware({
    event: req,
    responder: res,
    handler: (event, context) => buyProducts(event, context.userId, context.manager),
    auth: true,
    userRoles: [UserRole.CLIENT, UserRole.VENTURE],
  }).responseWrapped()
);
export default productRouter;
