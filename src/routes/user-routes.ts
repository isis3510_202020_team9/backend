import express from "express";
import { UserRole } from "../entity/User";
import { getOrder, logIn, profileUser, signUp } from "../middlewares/user-middleware";
import { getVenture } from "../middlewares/venture-middleware";
import { Middleware } from "../utils/middleware/Middleware";

enum UserRoutes {
  LOGIN = "/login",
  SIGNUP = "/signup",
  PROFILING = '/profiling',
  ORDERS = '/orders/:orderId',
  VENTURE = '/my-venture',
}

const userRouter = express.Router();

// Login route
userRouter.post(
  UserRoutes.LOGIN,
  async (req, res) =>
    await new Middleware({
      event: req,
      responder: res,
      handler: (event, context) => logIn(event, context.manager),
      auth: false,
    }).responseWrapped()
);

// Signup route
userRouter.post(
    UserRoutes.SIGNUP,
    async (req, res) =>
      await new Middleware({
        event: req,
        responder: res,
        handler: (event, context) => signUp(event, context.manager),
        auth: false,
      }).responseWrapped()
  );

  // POST users/profiling
  userRouter.post(
    UserRoutes.PROFILING,
    async (req, res) =>
      await new Middleware({
        event: req,
        responder: res,
        handler: (event, context) => profileUser(event, context.userId, context.manager),
        auth: true,
        userRoles: [UserRole.CLIENT]
      }).responseWrapped()
  );

  
  userRouter.get(
    UserRoutes.ORDERS,
    async (req, res) =>
      await new Middleware({
        event: req,
        responder: res,
        handler: (event, context) => getOrder(event, context.manager),
        auth: true,
        userRoles: [UserRole.CLIENT, UserRole.VENTURE]
      }).responseWrapped()
  );



  userRouter.get(
    UserRoutes.VENTURE,
    async (req, res) =>
      await new Middleware({
        event: req,
        responder: res,
        handler: (event, context) => getVenture(event, context.userId, context.manager),
        auth: true,
        userRoles: [UserRole.VENTURE]
      }).responseWrapped()
  );

  export default userRouter;
