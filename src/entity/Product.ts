import { Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";
import { OrderProducts } from "./OrderProducts";
import { User } from "./User";
import { Venture } from "./Venture";
import { Category } from "./VentureCategory";


/**
 * Class for venture
 */
@Entity()
export class Product {

    /**
     * Product id
     */
    @PrimaryGeneratedColumn("uuid")
    id: string;

    /**
     * Product registered name
     */
    @Column()
    name: string;

    /**
     * Phone number
     */
    @Column()
    prize: number;
  
  /**
   * Venture description
   */
    @Column({ type: 'text', nullable: true })
    description: string;
  
    /**
     * Product photo url
     */
    @Column({ nullable: true })
    photo: string;

    /**
     * Product category
     */
    @Column({ type: 'enum', enum: Category, default: Category.CLOTHING })
    productCategory: Category;

    
    /**
     * Tags defined by the user to be applied to the product
     */
    @Column({ nullable: true })
    tags: string;

    /**
     * Venture categories
     */
    @ManyToMany((type) => User, (user) => user.suggestedProducts)
    @JoinTable({ name: 'suggestedProducts'})
    users: User[];

    /**
     * Venture associated
     */
    @ManyToOne(() => Venture, venture => venture.products)
    venture: Venture;

    @OneToMany(() => OrderProducts, orderProduct => orderProduct.product)
    orders: OrderProducts[];

}