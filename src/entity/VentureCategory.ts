import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./User";
import { Venture } from "./Venture";

export enum Category {
    CLOTHING = 'CLOTHING',
    FAST_FOOD = 'FAST_FOOD',
    PETS = 'PETS',
    TOYS = 'TOYS',
    BAKERY = 'BAKERY',
    SPORTS = 'SPORTS',
}

/**
 * Venture category table
 */
@Entity()
export class VentureCategory {

    @PrimaryGeneratedColumn()
    id: string;

    /**
     * Venture category name
     */
    @Column({  type: 'enum', enum: Category, default: Category.FAST_FOOD, unique: true })
    name: Category;

    /**
     * Related ventures
     */
    @ManyToMany((type) => Venture, (venture) => venture.categories)
    ventures: Venture[];
}
