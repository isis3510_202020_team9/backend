import { Column, CreateDateColumn, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { OrderProducts } from "./OrderProducts";
import { Review } from "./Review";
import { User } from "./User";
import { Venture } from "./Venture";

/**
 * Order possible statuses
 */
export enum OrderStatus {
    NOT_STARTED = 'NOT_STARTED',
    IN_PROGRESS = 'IN_PROGRESS',
    ON_WAY = 'ON_WAY',
    FINISHED = 'FINISHED',
    DECLINED = 'DECLINED'
}
/**
 * Order made by an user to a venture
 */
@Entity()
export class Order {

    /**
     * Order id
     */
    @PrimaryGeneratedColumn("uuid")
    id: string;

    /**
     * Order user specifications and comments
     */
    @Column({ type: 'text', nullable: true })
    specifications: string;

    /**
     * Order final prize
     */
    @Column({ type: 'double precision', default: 0.0 })
    finalPrize: number;

    /**
     * Order desired date
     */
    @Column({ nullable: true })
    desiredDate: Date;

    /**
     * Order current status
     */
    @Column({ type: 'enum', enum: OrderStatus, default: OrderStatus.NOT_STARTED })
    status: OrderStatus;

    /**
     * Order products
     */
    @OneToMany(() => OrderProducts, orderProduct => orderProduct.order)
    products: OrderProducts[];

    /**
     * User that made the order
     */
    @ManyToOne(() => User, user => user.orders)
    user: User;

    /**
     * User that made the order
     */
    @ManyToOne(() => Venture, ve => ve.orders)
    venture: Venture;

    /**
     * Order address
     */
    @Column({
        nullable: true,
    })
    address: string;

    /**
     * Review associated to the order
     */
    @OneToOne((type) => Review, (review) => review.order, { eager: true })
    @JoinColumn()
    review: Review;

    @CreateDateColumn()
    createdAt: Date;
}