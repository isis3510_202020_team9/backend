import { Entity, PrimaryGeneratedColumn, Column, OneToOne } from "typeorm";
import { Order } from "./Order";
/**
 * Review from an order
 */
@Entity()
export class Review {

  @PrimaryGeneratedColumn()
  id: string;

  /**
   * User email
   */
  @Column({
      type: 'smallint'})
  grade: number;

  /**
   * Password encrypted field
   */
  @Column({type: 'text', nullable: true})
  comment: string;

  /**
   * Order associated
   */
  @OneToOne((type) => Order, (order) => order.review)
  order: Order;



}
