import { Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryColumn } from "typeorm";
import { Order } from "./Order";
import { Product } from "./Product";
import { User } from "./User";
import { VentureCategory } from "./VentureCategory";



/**
 * Class for venture
 */
@Entity()
export class Venture {

  /**
   * ID = User id
   */
  @PrimaryColumn()
  id: string;

  /**
   * Venture registered name
   */
  @Column()
  name: string;

  /**
   * Phone number
   */
  @Column()
  phone: string;

  /**
   * Venture ddress
   */
  @Column()
  address: string;

  /**
   * Url from its Facebook
   */
  @Column({ nullable: true })
  facebookUrl: string;

  /**
   * Url from its Instagram
   */
  @Column({ nullable: true })
  instagramUrl: string;

  /**
   * Venture categories
   */
  @ManyToMany((type) => VentureCategory, (category) => category.ventures)
  @JoinTable({ name: 'venturesCategories' })
  categories: VentureCategory[];

  /**
   * Venture categories
   */
  @ManyToMany((type) => User, (user) => user.suggestedVentures)
  @JoinTable({ name: 'suggestedVentures' })
  users: User[];

  /**
   * Products
   */
  @OneToMany(() => Product, product => product.venture)
  products: Product[];

  /**
   * Orders created by a user
   */
  @OneToMany(() => Order, order => order.venture)
  orders: Order[];

  /**
   * Venture latitude
   */
  @Column({ type: 'double precision' })
  latitude: number;

  /**
   * Venture longitude
   */
  @Column({ type: 'double precision' })
  longitude: number;

  /**
   * Venture photo url
   */
  @Column({ nullable: true })
  photo: string;

  /**
   * Venture rating
   */
  @Column({ nullable: true })
  rating: number;

}


export class VentureReduced {

  /**
   * ID = User id
   */
  id: string = '';

  /**
   * Venture registered name
   */
  name: string = '';

  /**
   * Phone number
   */
  phone: string = '';

  /**
   * Venture ddress
   */
  address: string = '';

  /**
   * Url from its Facebook
   */
  facebookUrl: string = '';

  /**
   * Url from its Instagram
   */
  instagramUrl: string = '';

  /**
   * Venture categories
   */
  categories: VentureCategory[] = [];

  /**
   * Venture latitude
   */
  latitude: number = 0;

  /**
   * Venture longitude
   */
  longitude: number = 0;

  /**
   * Venture photo url
   */
  photo: string = '';
}