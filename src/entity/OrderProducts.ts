import { Column, CreateDateColumn, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";
import { Order } from "./Order";
import { Product } from "./Product";
/**
 * Order made by an user to a venture
 */
@Entity()
export class OrderProducts {
    // Relation id
    @PrimaryGeneratedColumn('uuid')
    id: string;  
    /**
     * User that made the order
     */
    @ManyToOne(type => Product, product => product.orders)
    product: Product;

    /**
     * User that made the order
     */
    @ManyToOne(type => Order, or => or.products)
    order: Order;

    /**
     * Units of the product
    */
    @Column({ default: 0 })
    units: number;
}