import { Entity, PrimaryGeneratedColumn, Column, ManyToMany, OneToMany, JoinTable, Index } from "typeorm";
import { Order } from "./Order";
import { Product } from "./Product";
import { Venture } from "./Venture";
import { VentureCategory } from "./VentureCategory";

/**
 * User possible roles
 * VENTURE -> Is also a client but has more privileges
 */
export enum UserRole {
  VENTURE = 'VENTURE',
  CLIENT = 'CLIENT'
}

/**
 * User of creatur
 */
@Entity()
export class User {

  @PrimaryGeneratedColumn("uuid")
  id: string;

  /**
   * User email
   */
  @Column({
    type: "varchar",
    length: 100,
    unique: true,
  })
  @Index()
  email: string;

  /**
   * Password encrypted field
   */
  @Column()
  password: string;

  /**
   * User role
   */
  @Column({ type: 'enum', enum: UserRole, default: UserRole.CLIENT })
  role: UserRole;

  /**
   * Suggested ventures
   */
  @ManyToMany((type) => Venture, (venture) => venture.users, { cascade: true , onUpdate: "CASCADE"})
  suggestedVentures: Venture[];

    /**
   * Suggested products
   */
  @ManyToMany((type) => Product, (product) => product.users, { cascade: true , onUpdate: "CASCADE"})
  suggestedProducts: Product[];


  /**
   * Orders created by a user
   */
  @OneToMany(() => Order, order => order.user)
  orders: Order[];

  /**
   * User latitude
   */
  @Column({ type: 'double precision', nullable: true })
  latitude: number;

  /**
   * User longitude
   */
  @Column({ type: 'double precision', nullable: true })
  longitude: number;

  /**
   * User birthdate
   */
  @Column({ type: 'date', nullable: true })
  birthdate: Date;

  /**
   * Has profiling 
   */
  @Column({ default: false })
  hasProfiling: boolean;

}
