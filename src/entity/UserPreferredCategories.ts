import { Entity, PrimaryColumn } from "typeorm";
import { Category } from "./VentureCategory";

/**
 * Venture category table
 */
@Entity()
export class UserPreferredCategory {

    @PrimaryColumn()
    user_id: string;

    /**
     * Venture category name
     */
    @PrimaryColumn({ type: 'enum', enum: Category, default: Category.CLOTHING })
    category: Category;
}
