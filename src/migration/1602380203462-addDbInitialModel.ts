import {MigrationInterface, QueryRunner} from "typeorm";

export class addDbInitialModel1602380203462 implements MigrationInterface {
    name = 'addDbInitialModel1602380203462'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "venture_category_name_enum" AS ENUM('CLOTHING', 'WOMEN_ACCESORIES', 'FAST_FOOD', 'PETS', 'SHOES', 'BAKERY')`);
        await queryRunner.query(`CREATE TABLE "venture_category" ("id" SERIAL NOT NULL, "name" "venture_category_name_enum" NOT NULL DEFAULT 'FAST_FOOD', CONSTRAINT "UQ_9bcfeb333bb4af87b7fd3bdeb13" UNIQUE ("name"), CONSTRAINT "PK_080ff1d61711c66ff3d0d0047a3" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "venture" ("id" character varying NOT NULL, "name" character varying NOT NULL, "phone" character varying NOT NULL, "address" character varying NOT NULL, "facebook_url" character varying, "instagram_url" character varying, CONSTRAINT "PK_a0c1eb7cf68fcd445c8e95e54cd" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TYPE "product_product_category_enum" AS ENUM('CLOTHING', 'WOMEN_ACCESORIES', 'FAST_FOOD', 'PETS', 'SHOES', 'BAKERY')`);
        await queryRunner.query(`CREATE TABLE "product" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "prize" integer NOT NULL, "description" text, "units" integer NOT NULL DEFAULT 0, "photo" character varying, "product_category" "product_product_category_enum" NOT NULL DEFAULT 'CLOTHING', "tags" character varying, "venture_id" character varying, CONSTRAINT "PK_bebc9158e480b949565b4dc7a82" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "review" ("id" SERIAL NOT NULL, "grade" smallint NOT NULL, "comment" text, CONSTRAINT "PK_2e4299a343a81574217255c00ca" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TYPE "order_status_enum" AS ENUM('NOT_STARTED', 'IN_PROGRESS', 'FINISHED', 'DECLINED')`);
        await queryRunner.query(`CREATE TABLE "order" ("id" SERIAL NOT NULL, "specifications" text, "final_prize" double precision NOT NULL DEFAULT 0, "desired_date" TIMESTAMP, "status" "order_status_enum" NOT NULL DEFAULT 'NOT_STARTED', "user_id" integer, "venture_id" character varying, "review_id" integer, CONSTRAINT "REL_59c426f683eb876b8be2f033fd" UNIQUE ("review_id"), CONSTRAINT "PK_1031171c13130102495201e3e20" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "venturesCategories" ("venture_id" character varying NOT NULL, "venture_category_id" integer NOT NULL, CONSTRAINT "PK_1c4d89cb1dc1a4aba60a30c7ccb" PRIMARY KEY ("venture_id", "venture_category_id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_58325c26ab1bf59f4fd27970ff" ON "venturesCategories" ("venture_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_887e52ccc773d0fb5e87e00db7" ON "venturesCategories" ("venture_category_id") `);
        await queryRunner.query(`CREATE TABLE "suggestedVentures" ("venture_id" character varying NOT NULL, "user_id" integer NOT NULL, CONSTRAINT "PK_cafc22776f66949908f87e09470" PRIMARY KEY ("venture_id", "user_id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_cf92e8cf90932f2f17f3a9e071" ON "suggestedVentures" ("venture_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_814c618ce430b7f6a9fc037e59" ON "suggestedVentures" ("user_id") `);
        await queryRunner.query(`CREATE TABLE "suggestedProducts" ("product_id" integer NOT NULL, "user_id" integer NOT NULL, CONSTRAINT "PK_a56fbaebbc71737aca06acb5d2e" PRIMARY KEY ("product_id", "user_id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_9b489723af3c817c13d64151a8" ON "suggestedProducts" ("product_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_d5a254dc1eea54c4232a6e22e5" ON "suggestedProducts" ("user_id") `);
        await queryRunner.query(`CREATE TABLE "productsOrder" ("order_id" integer NOT NULL, "product_id" integer NOT NULL, CONSTRAINT "PK_4d97aa393b327e0de999a8d7444" PRIMARY KEY ("order_id", "product_id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_85f07b96e819b141286761a441" ON "productsOrder" ("order_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_d54349c19b5ae0602de8941e54" ON "productsOrder" ("product_id") `);
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "PK_cace4a159ff9f2512dd42373760"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "id" SERIAL NOT NULL`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TYPE "public"."user_role_enum" RENAME TO "user_role_enum_old"`);
        await queryRunner.query(`CREATE TYPE "user_role_enum" AS ENUM('VENTURE', 'CLIENT')`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "role" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "role" TYPE "user_role_enum" USING "role"::"text"::"user_role_enum"`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "role" SET DEFAULT 'CLIENT'`);
        await queryRunner.query(`DROP TYPE "user_role_enum_old"`);
        await queryRunner.query(`ALTER TABLE "product" ADD CONSTRAINT "FK_29fc9216c99ae5242765e7e128d" FOREIGN KEY ("venture_id") REFERENCES "venture"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "order" ADD CONSTRAINT "FK_199e32a02ddc0f47cd93181d8fd" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "order" ADD CONSTRAINT "FK_120ddace467f627d2a33c45c6fc" FOREIGN KEY ("venture_id") REFERENCES "venture"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "order" ADD CONSTRAINT "FK_59c426f683eb876b8be2f033fd3" FOREIGN KEY ("review_id") REFERENCES "review"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "venturesCategories" ADD CONSTRAINT "FK_58325c26ab1bf59f4fd27970ffa" FOREIGN KEY ("venture_id") REFERENCES "venture"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "venturesCategories" ADD CONSTRAINT "FK_887e52ccc773d0fb5e87e00db7a" FOREIGN KEY ("venture_category_id") REFERENCES "venture_category"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "suggestedVentures" ADD CONSTRAINT "FK_cf92e8cf90932f2f17f3a9e0710" FOREIGN KEY ("venture_id") REFERENCES "venture"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "suggestedVentures" ADD CONSTRAINT "FK_814c618ce430b7f6a9fc037e596" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" ADD CONSTRAINT "FK_9b489723af3c817c13d64151a8e" FOREIGN KEY ("product_id") REFERENCES "product"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" ADD CONSTRAINT "FK_d5a254dc1eea54c4232a6e22e5a" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "productsOrder" ADD CONSTRAINT "FK_85f07b96e819b141286761a4410" FOREIGN KEY ("order_id") REFERENCES "order"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "productsOrder" ADD CONSTRAINT "FK_d54349c19b5ae0602de8941e540" FOREIGN KEY ("product_id") REFERENCES "product"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "productsOrder" DROP CONSTRAINT "FK_d54349c19b5ae0602de8941e540"`);
        await queryRunner.query(`ALTER TABLE "productsOrder" DROP CONSTRAINT "FK_85f07b96e819b141286761a4410"`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" DROP CONSTRAINT "FK_d5a254dc1eea54c4232a6e22e5a"`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" DROP CONSTRAINT "FK_9b489723af3c817c13d64151a8e"`);
        await queryRunner.query(`ALTER TABLE "suggestedVentures" DROP CONSTRAINT "FK_814c618ce430b7f6a9fc037e596"`);
        await queryRunner.query(`ALTER TABLE "suggestedVentures" DROP CONSTRAINT "FK_cf92e8cf90932f2f17f3a9e0710"`);
        await queryRunner.query(`ALTER TABLE "venturesCategories" DROP CONSTRAINT "FK_887e52ccc773d0fb5e87e00db7a"`);
        await queryRunner.query(`ALTER TABLE "venturesCategories" DROP CONSTRAINT "FK_58325c26ab1bf59f4fd27970ffa"`);
        await queryRunner.query(`ALTER TABLE "order" DROP CONSTRAINT "FK_59c426f683eb876b8be2f033fd3"`);
        await queryRunner.query(`ALTER TABLE "order" DROP CONSTRAINT "FK_120ddace467f627d2a33c45c6fc"`);
        await queryRunner.query(`ALTER TABLE "order" DROP CONSTRAINT "FK_199e32a02ddc0f47cd93181d8fd"`);
        await queryRunner.query(`ALTER TABLE "product" DROP CONSTRAINT "FK_29fc9216c99ae5242765e7e128d"`);
        await queryRunner.query(`CREATE TYPE "user_role_enum_old" AS ENUM('VENTUR', 'CLIENT')`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "role" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "role" TYPE "user_role_enum_old" USING "role"::"text"::"user_role_enum_old"`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "role" SET DEFAULT 'CLIENT'`);
        await queryRunner.query(`DROP TYPE "user_role_enum"`);
        await queryRunner.query(`ALTER TYPE "user_role_enum_old" RENAME TO  "user_role_enum"`);
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "PK_cace4a159ff9f2512dd42373760"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "id" uuid NOT NULL DEFAULT uuid_generate_v4()`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id")`);
        await queryRunner.query(`DROP INDEX "IDX_d54349c19b5ae0602de8941e54"`);
        await queryRunner.query(`DROP INDEX "IDX_85f07b96e819b141286761a441"`);
        await queryRunner.query(`DROP TABLE "productsOrder"`);
        await queryRunner.query(`DROP INDEX "IDX_d5a254dc1eea54c4232a6e22e5"`);
        await queryRunner.query(`DROP INDEX "IDX_9b489723af3c817c13d64151a8"`);
        await queryRunner.query(`DROP TABLE "suggestedProducts"`);
        await queryRunner.query(`DROP INDEX "IDX_814c618ce430b7f6a9fc037e59"`);
        await queryRunner.query(`DROP INDEX "IDX_cf92e8cf90932f2f17f3a9e071"`);
        await queryRunner.query(`DROP TABLE "suggestedVentures"`);
        await queryRunner.query(`DROP INDEX "IDX_887e52ccc773d0fb5e87e00db7"`);
        await queryRunner.query(`DROP INDEX "IDX_58325c26ab1bf59f4fd27970ff"`);
        await queryRunner.query(`DROP TABLE "venturesCategories"`);
        await queryRunner.query(`DROP TABLE "order"`);
        await queryRunner.query(`DROP TYPE "order_status_enum"`);
        await queryRunner.query(`DROP TABLE "review"`);
        await queryRunner.query(`DROP TABLE "product"`);
        await queryRunner.query(`DROP TYPE "product_product_category_enum"`);
        await queryRunner.query(`DROP TABLE "venture"`);
        await queryRunner.query(`DROP TABLE "venture_category"`);
        await queryRunner.query(`DROP TYPE "venture_category_name_enum"`);
    }

}
