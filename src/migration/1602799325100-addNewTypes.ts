import {MigrationInterface, QueryRunner} from "typeorm";

export class addNewTypes1602799325100 implements MigrationInterface {
    name = 'addNewTypes1602799325100'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TYPE "public"."venture_category_name_enum" RENAME TO "venture_category_name_enum_old"`);
        await queryRunner.query(`CREATE TYPE "venture_category_name_enum" AS ENUM('CLOTHING', 'FAST_FOOD', 'PETS', 'TOYS', 'BAKERY', 'SPORTS')`);
        await queryRunner.query(`ALTER TABLE "venture_category" ALTER COLUMN "name" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "venture_category" ALTER COLUMN "name" TYPE "venture_category_name_enum" USING "name"::"text"::"venture_category_name_enum"`);
        await queryRunner.query(`ALTER TABLE "venture_category" ALTER COLUMN "name" SET DEFAULT 'FAST_FOOD'`);
        await queryRunner.query(`DROP TYPE "venture_category_name_enum_old"`);
        await queryRunner.query(`ALTER TYPE "public"."product_product_category_enum" RENAME TO "product_product_category_enum_old"`);
        await queryRunner.query(`CREATE TYPE "product_product_category_enum" AS ENUM('CLOTHING', 'FAST_FOOD', 'PETS', 'TOYS', 'BAKERY', 'SPORTS')`);
        await queryRunner.query(`ALTER TABLE "product" ALTER COLUMN "product_category" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "product" ALTER COLUMN "product_category" TYPE "product_product_category_enum" USING "product_category"::"text"::"product_product_category_enum"`);
        await queryRunner.query(`ALTER TABLE "product" ALTER COLUMN "product_category" SET DEFAULT 'CLOTHING'`);
        await queryRunner.query(`DROP TYPE "product_product_category_enum_old"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "product_product_category_enum_old" AS ENUM('CLOTHING', 'WOMEN_ACCESORIES', 'FAST_FOOD', 'PETS', 'SHOES', 'BAKERY')`);
        await queryRunner.query(`ALTER TABLE "product" ALTER COLUMN "product_category" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "product" ALTER COLUMN "product_category" TYPE "product_product_category_enum_old" USING "product_category"::"text"::"product_product_category_enum_old"`);
        await queryRunner.query(`ALTER TABLE "product" ALTER COLUMN "product_category" SET DEFAULT 'CLOTHING'`);
        await queryRunner.query(`DROP TYPE "product_product_category_enum"`);
        await queryRunner.query(`ALTER TYPE "product_product_category_enum_old" RENAME TO  "product_product_category_enum"`);
        await queryRunner.query(`CREATE TYPE "venture_category_name_enum_old" AS ENUM('CLOTHING', 'WOMEN_ACCESORIES', 'FAST_FOOD', 'PETS', 'SHOES', 'BAKERY')`);
        await queryRunner.query(`ALTER TABLE "venture_category" ALTER COLUMN "name" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "venture_category" ALTER COLUMN "name" TYPE "venture_category_name_enum_old" USING "name"::"text"::"venture_category_name_enum_old"`);
        await queryRunner.query(`ALTER TABLE "venture_category" ALTER COLUMN "name" SET DEFAULT 'FAST_FOOD'`);
        await queryRunner.query(`DROP TYPE "venture_category_name_enum"`);
        await queryRunner.query(`ALTER TYPE "venture_category_name_enum_old" RENAME TO  "venture_category_name_enum"`);
    }

}
