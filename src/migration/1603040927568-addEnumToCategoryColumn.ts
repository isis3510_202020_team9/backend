import {MigrationInterface, QueryRunner} from "typeorm";

export class addEnumToCategoryColumn1603040927568 implements MigrationInterface {
    name = 'addEnumToCategoryColumn1603040927568'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_preferred_category" DROP CONSTRAINT "PK_2de689a7d8901c8224a04afedcc"`);
        await queryRunner.query(`ALTER TABLE "user_preferred_category" ADD CONSTRAINT "PK_ee08324758f53b985941b453c8d" PRIMARY KEY ("user_id")`);
        await queryRunner.query(`ALTER TABLE "user_preferred_category" DROP COLUMN "category"`);
        await queryRunner.query(`CREATE TYPE "user_preferred_category_category_enum" AS ENUM('CLOTHING', 'FAST_FOOD', 'PETS', 'TOYS', 'BAKERY', 'SPORTS')`);
        await queryRunner.query(`ALTER TABLE "user_preferred_category" ADD "category" "user_preferred_category_category_enum" NOT NULL DEFAULT 'CLOTHING'`);
        await queryRunner.query(`ALTER TABLE "user_preferred_category" DROP CONSTRAINT "PK_ee08324758f53b985941b453c8d"`);
        await queryRunner.query(`ALTER TABLE "user_preferred_category" ADD CONSTRAINT "PK_2de689a7d8901c8224a04afedcc" PRIMARY KEY ("user_id", "category")`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_preferred_category" DROP CONSTRAINT "PK_2de689a7d8901c8224a04afedcc"`);
        await queryRunner.query(`ALTER TABLE "user_preferred_category" ADD CONSTRAINT "PK_ee08324758f53b985941b453c8d" PRIMARY KEY ("user_id")`);
        await queryRunner.query(`ALTER TABLE "user_preferred_category" DROP COLUMN "category"`);
        await queryRunner.query(`DROP TYPE "user_preferred_category_category_enum"`);
        await queryRunner.query(`ALTER TABLE "user_preferred_category" ADD "category" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "user_preferred_category" DROP CONSTRAINT "PK_ee08324758f53b985941b453c8d"`);
        await queryRunner.query(`ALTER TABLE "user_preferred_category" ADD CONSTRAINT "PK_2de689a7d8901c8224a04afedcc" PRIMARY KEY ("user_id", "category")`);
    }

}
