import {MigrationInterface, QueryRunner} from "typeorm";

export class changeIdToUuidForUsr1603079503571 implements MigrationInterface {
    name = 'changeIdToUuidForUsr1603079503571'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "order" DROP CONSTRAINT "FK_199e32a02ddc0f47cd93181d8fd"`);
        await queryRunner.query(`ALTER TABLE "suggestedVentures" DROP CONSTRAINT "FK_814c618ce430b7f6a9fc037e596"`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" DROP CONSTRAINT "FK_d5a254dc1eea54c4232a6e22e5a"`);
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "PK_cace4a159ff9f2512dd42373760"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "id" uuid NOT NULL DEFAULT uuid_generate_v4()`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "order" DROP COLUMN "user_id"`);
        await queryRunner.query(`ALTER TABLE "order" ADD "user_id" uuid`);
        await queryRunner.query(`ALTER TABLE "suggestedVentures" DROP CONSTRAINT "PK_cafc22776f66949908f87e09470"`);
        await queryRunner.query(`ALTER TABLE "suggestedVentures" ADD CONSTRAINT "PK_cf92e8cf90932f2f17f3a9e0710" PRIMARY KEY ("venture_id")`);
        await queryRunner.query(`DROP INDEX "IDX_814c618ce430b7f6a9fc037e59"`);
        await queryRunner.query(`ALTER TABLE "suggestedVentures" DROP COLUMN "user_id"`);
        await queryRunner.query(`ALTER TABLE "suggestedVentures" ADD "user_id" uuid NOT NULL`);
        await queryRunner.query(`ALTER TABLE "suggestedVentures" DROP CONSTRAINT "PK_cf92e8cf90932f2f17f3a9e0710"`);
        await queryRunner.query(`ALTER TABLE "suggestedVentures" ADD CONSTRAINT "PK_cafc22776f66949908f87e09470" PRIMARY KEY ("venture_id", "user_id")`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" DROP CONSTRAINT "PK_a56fbaebbc71737aca06acb5d2e"`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" ADD CONSTRAINT "PK_9b489723af3c817c13d64151a8e" PRIMARY KEY ("product_id")`);
        await queryRunner.query(`DROP INDEX "IDX_d5a254dc1eea54c4232a6e22e5"`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" DROP COLUMN "user_id"`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" ADD "user_id" uuid NOT NULL`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" DROP CONSTRAINT "PK_9b489723af3c817c13d64151a8e"`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" ADD CONSTRAINT "PK_a56fbaebbc71737aca06acb5d2e" PRIMARY KEY ("product_id", "user_id")`);
        await queryRunner.query(`CREATE INDEX "IDX_814c618ce430b7f6a9fc037e59" ON "suggestedVentures" ("user_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_d5a254dc1eea54c4232a6e22e5" ON "suggestedProducts" ("user_id") `);
        await queryRunner.query(`ALTER TABLE "order" ADD CONSTRAINT "FK_199e32a02ddc0f47cd93181d8fd" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "suggestedVentures" ADD CONSTRAINT "FK_814c618ce430b7f6a9fc037e596" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" ADD CONSTRAINT "FK_d5a254dc1eea54c4232a6e22e5a" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "suggestedProducts" DROP CONSTRAINT "FK_d5a254dc1eea54c4232a6e22e5a"`);
        await queryRunner.query(`ALTER TABLE "suggestedVentures" DROP CONSTRAINT "FK_814c618ce430b7f6a9fc037e596"`);
        await queryRunner.query(`ALTER TABLE "order" DROP CONSTRAINT "FK_199e32a02ddc0f47cd93181d8fd"`);
        await queryRunner.query(`DROP INDEX "IDX_d5a254dc1eea54c4232a6e22e5"`);
        await queryRunner.query(`DROP INDEX "IDX_814c618ce430b7f6a9fc037e59"`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" DROP CONSTRAINT "PK_a56fbaebbc71737aca06acb5d2e"`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" ADD CONSTRAINT "PK_9b489723af3c817c13d64151a8e" PRIMARY KEY ("product_id")`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" DROP COLUMN "user_id"`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" ADD "user_id" integer NOT NULL`);
        await queryRunner.query(`CREATE INDEX "IDX_d5a254dc1eea54c4232a6e22e5" ON "suggestedProducts" ("user_id") `);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" DROP CONSTRAINT "PK_9b489723af3c817c13d64151a8e"`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" ADD CONSTRAINT "PK_a56fbaebbc71737aca06acb5d2e" PRIMARY KEY ("user_id", "product_id")`);
        await queryRunner.query(`ALTER TABLE "suggestedVentures" DROP CONSTRAINT "PK_cafc22776f66949908f87e09470"`);
        await queryRunner.query(`ALTER TABLE "suggestedVentures" ADD CONSTRAINT "PK_cf92e8cf90932f2f17f3a9e0710" PRIMARY KEY ("venture_id")`);
        await queryRunner.query(`ALTER TABLE "suggestedVentures" DROP COLUMN "user_id"`);
        await queryRunner.query(`ALTER TABLE "suggestedVentures" ADD "user_id" integer NOT NULL`);
        await queryRunner.query(`CREATE INDEX "IDX_814c618ce430b7f6a9fc037e59" ON "suggestedVentures" ("user_id") `);
        await queryRunner.query(`ALTER TABLE "suggestedVentures" DROP CONSTRAINT "PK_cf92e8cf90932f2f17f3a9e0710"`);
        await queryRunner.query(`ALTER TABLE "suggestedVentures" ADD CONSTRAINT "PK_cafc22776f66949908f87e09470" PRIMARY KEY ("venture_id", "user_id")`);
        await queryRunner.query(`ALTER TABLE "order" DROP COLUMN "user_id"`);
        await queryRunner.query(`ALTER TABLE "order" ADD "user_id" integer`);
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "PK_cace4a159ff9f2512dd42373760"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "id" SERIAL NOT NULL`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" ADD CONSTRAINT "FK_d5a254dc1eea54c4232a6e22e5a" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "suggestedVentures" ADD CONSTRAINT "FK_814c618ce430b7f6a9fc037e596" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "order" ADD CONSTRAINT "FK_199e32a02ddc0f47cd93181d8fd" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
