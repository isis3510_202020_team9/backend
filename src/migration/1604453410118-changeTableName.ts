import {MigrationInterface, QueryRunner} from "typeorm";

export class changeTableName1604453410118 implements MigrationInterface {
    name = 'changeTableName1604453410118'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "products_order" ("order_id" integer NOT NULL, "product_id" uuid NOT NULL, CONSTRAINT "PK_3a7ed7f4883f3e333494792d899" PRIMARY KEY ("order_id", "product_id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_86d624ce6ee01da2927b099b2d" ON "products_order" ("order_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_c7df5e7f12bad1b2f862c95bc8" ON "products_order" ("product_id") `);
        await queryRunner.query(`ALTER TABLE "order" ADD "created_at" TIMESTAMP NOT NULL DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "products_order" ADD CONSTRAINT "FK_86d624ce6ee01da2927b099b2d3" FOREIGN KEY ("order_id") REFERENCES "order"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "products_order" ADD CONSTRAINT "FK_c7df5e7f12bad1b2f862c95bc87" FOREIGN KEY ("product_id") REFERENCES "product"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "productsOrder" DROP CONSTRAINT "FK_d54349c19b5ae0602de8941e540"`);
        await queryRunner.query(`ALTER TABLE "productsOrder" DROP CONSTRAINT "FK_85f07b96e819b141286761a4410"`);
        await queryRunner.query(`DROP TABLE "productsOrder"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "products_order" DROP CONSTRAINT "FK_c7df5e7f12bad1b2f862c95bc87"`);
        await queryRunner.query(`ALTER TABLE "products_order" DROP CONSTRAINT "FK_86d624ce6ee01da2927b099b2d3"`);
        await queryRunner.query(`ALTER TABLE "order" DROP COLUMN "created_at"`);
        await queryRunner.query(`DROP INDEX "IDX_c7df5e7f12bad1b2f862c95bc8"`);
        await queryRunner.query(`DROP INDEX "IDX_86d624ce6ee01da2927b099b2d"`);
        await queryRunner.query(`DROP TABLE "products_order"`);
    }

}
