import {MigrationInterface, QueryRunner} from "typeorm";

export class addBirthdateAndPreferredCat1603037412094 implements MigrationInterface {
    name = 'addBirthdateAndPreferredCat1603037412094'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "user_preferred_category" ("user_id" character varying NOT NULL, "category" character varying NOT NULL, CONSTRAINT "PK_2de689a7d8901c8224a04afedcc" PRIMARY KEY ("user_id", "category"))`);
        await queryRunner.query(`ALTER TABLE "venture" ADD "photo" character varying`);
        await queryRunner.query(`ALTER TABLE "user" ADD "birthdate" date`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "birthdate"`);
        await queryRunner.query(`ALTER TABLE "venture" DROP COLUMN "photo"`);
        await queryRunner.query(`DROP TABLE "user_preferred_category"`);
    }

}
