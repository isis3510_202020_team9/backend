import {MigrationInterface, QueryRunner} from "typeorm";

export class addOnwayStatus1605391543808 implements MigrationInterface {
    name = 'addOnwayStatus1605391543808'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TYPE "public"."order_status_enum" RENAME TO "order_status_enum_old"`);
        await queryRunner.query(`CREATE TYPE "order_status_enum" AS ENUM('NOT_STARTED', 'IN_PROGRESS', 'ON_WAY', 'FINISHED', 'DECLINED')`);
        await queryRunner.query(`ALTER TABLE "order" ALTER COLUMN "status" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "order" ALTER COLUMN "status" TYPE "order_status_enum" USING "status"::"text"::"order_status_enum"`);
        await queryRunner.query(`ALTER TABLE "order" ALTER COLUMN "status" SET DEFAULT 'NOT_STARTED'`);
        await queryRunner.query(`DROP TYPE "order_status_enum_old"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "order_status_enum_old" AS ENUM('NOT_STARTED', 'IN_PROGRESS', 'FINISHED', 'DECLINED')`);
        await queryRunner.query(`ALTER TABLE "order" ALTER COLUMN "status" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "order" ALTER COLUMN "status" TYPE "order_status_enum_old" USING "status"::"text"::"order_status_enum_old"`);
        await queryRunner.query(`ALTER TABLE "order" ALTER COLUMN "status" SET DEFAULT 'NOT_STARTED'`);
        await queryRunner.query(`DROP TYPE "order_status_enum"`);
        await queryRunner.query(`ALTER TYPE "order_status_enum_old" RENAME TO  "order_status_enum"`);
    }

}
