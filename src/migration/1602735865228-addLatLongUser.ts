import {MigrationInterface, QueryRunner} from "typeorm";

export class addLatLongUser1602735865228 implements MigrationInterface {
    name = 'addLatLongUser1602735865228'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" ADD "latitude" double precision`);
        await queryRunner.query(`ALTER TABLE "user" ADD "longitude" double precision`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "longitude"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "latitude"`);
    }

}
