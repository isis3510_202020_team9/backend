import {MigrationInterface, QueryRunner} from "typeorm";

export class addOrderAddress1604897890263 implements MigrationInterface {
    name = 'addOrderAddress1604897890263'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "order" ADD "address" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "order" DROP COLUMN "address"`);
    }

}
