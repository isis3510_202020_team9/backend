import {MigrationInterface, QueryRunner} from "typeorm";

export class changeProductIduuid1602733695572 implements MigrationInterface {
    name = 'changeProductIduuid1602733695572'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "suggestedProducts" DROP CONSTRAINT "FK_9b489723af3c817c13d64151a8e"`);
        await queryRunner.query(`ALTER TABLE "productsOrder" DROP CONSTRAINT "FK_d54349c19b5ae0602de8941e540"`);
        await queryRunner.query(`ALTER TABLE "product" DROP CONSTRAINT "PK_bebc9158e480b949565b4dc7a82"`);
        await queryRunner.query(`ALTER TABLE "product" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "product" ADD "id" uuid NOT NULL DEFAULT uuid_generate_v4()`);
        await queryRunner.query(`ALTER TABLE "product" ADD CONSTRAINT "PK_bebc9158e480b949565b4dc7a82" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" DROP CONSTRAINT "PK_a56fbaebbc71737aca06acb5d2e"`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" ADD CONSTRAINT "PK_d5a254dc1eea54c4232a6e22e5a" PRIMARY KEY ("user_id")`);
        await queryRunner.query(`DROP INDEX "IDX_9b489723af3c817c13d64151a8"`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" DROP COLUMN "product_id"`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" ADD "product_id" uuid NOT NULL`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" DROP CONSTRAINT "PK_d5a254dc1eea54c4232a6e22e5a"`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" ADD CONSTRAINT "PK_a56fbaebbc71737aca06acb5d2e" PRIMARY KEY ("user_id", "product_id")`);
        await queryRunner.query(`ALTER TABLE "productsOrder" DROP CONSTRAINT "PK_4d97aa393b327e0de999a8d7444"`);
        await queryRunner.query(`ALTER TABLE "productsOrder" ADD CONSTRAINT "PK_85f07b96e819b141286761a4410" PRIMARY KEY ("order_id")`);
        await queryRunner.query(`DROP INDEX "IDX_d54349c19b5ae0602de8941e54"`);
        await queryRunner.query(`ALTER TABLE "productsOrder" DROP COLUMN "product_id"`);
        await queryRunner.query(`ALTER TABLE "productsOrder" ADD "product_id" uuid NOT NULL`);
        await queryRunner.query(`ALTER TABLE "productsOrder" DROP CONSTRAINT "PK_85f07b96e819b141286761a4410"`);
        await queryRunner.query(`ALTER TABLE "productsOrder" ADD CONSTRAINT "PK_4d97aa393b327e0de999a8d7444" PRIMARY KEY ("order_id", "product_id")`);
        await queryRunner.query(`CREATE INDEX "IDX_9b489723af3c817c13d64151a8" ON "suggestedProducts" ("product_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_d54349c19b5ae0602de8941e54" ON "productsOrder" ("product_id") `);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" ADD CONSTRAINT "FK_9b489723af3c817c13d64151a8e" FOREIGN KEY ("product_id") REFERENCES "product"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "productsOrder" ADD CONSTRAINT "FK_d54349c19b5ae0602de8941e540" FOREIGN KEY ("product_id") REFERENCES "product"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "productsOrder" DROP CONSTRAINT "FK_d54349c19b5ae0602de8941e540"`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" DROP CONSTRAINT "FK_9b489723af3c817c13d64151a8e"`);
        await queryRunner.query(`DROP INDEX "IDX_d54349c19b5ae0602de8941e54"`);
        await queryRunner.query(`DROP INDEX "IDX_9b489723af3c817c13d64151a8"`);
        await queryRunner.query(`ALTER TABLE "productsOrder" DROP CONSTRAINT "PK_4d97aa393b327e0de999a8d7444"`);
        await queryRunner.query(`ALTER TABLE "productsOrder" ADD CONSTRAINT "PK_85f07b96e819b141286761a4410" PRIMARY KEY ("order_id")`);
        await queryRunner.query(`ALTER TABLE "productsOrder" DROP COLUMN "product_id"`);
        await queryRunner.query(`ALTER TABLE "productsOrder" ADD "product_id" integer NOT NULL`);
        await queryRunner.query(`CREATE INDEX "IDX_d54349c19b5ae0602de8941e54" ON "productsOrder" ("product_id") `);
        await queryRunner.query(`ALTER TABLE "productsOrder" DROP CONSTRAINT "PK_85f07b96e819b141286761a4410"`);
        await queryRunner.query(`ALTER TABLE "productsOrder" ADD CONSTRAINT "PK_4d97aa393b327e0de999a8d7444" PRIMARY KEY ("order_id", "product_id")`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" DROP CONSTRAINT "PK_a56fbaebbc71737aca06acb5d2e"`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" ADD CONSTRAINT "PK_d5a254dc1eea54c4232a6e22e5a" PRIMARY KEY ("user_id")`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" DROP COLUMN "product_id"`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" ADD "product_id" integer NOT NULL`);
        await queryRunner.query(`CREATE INDEX "IDX_9b489723af3c817c13d64151a8" ON "suggestedProducts" ("product_id") `);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" DROP CONSTRAINT "PK_d5a254dc1eea54c4232a6e22e5a"`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" ADD CONSTRAINT "PK_a56fbaebbc71737aca06acb5d2e" PRIMARY KEY ("product_id", "user_id")`);
        await queryRunner.query(`ALTER TABLE "product" DROP CONSTRAINT "PK_bebc9158e480b949565b4dc7a82"`);
        await queryRunner.query(`ALTER TABLE "product" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "product" ADD "id" SERIAL NOT NULL`);
        await queryRunner.query(`ALTER TABLE "product" ADD CONSTRAINT "PK_bebc9158e480b949565b4dc7a82" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "productsOrder" ADD CONSTRAINT "FK_d54349c19b5ae0602de8941e540" FOREIGN KEY ("product_id") REFERENCES "product"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "suggestedProducts" ADD CONSTRAINT "FK_9b489723af3c817c13d64151a8e" FOREIGN KEY ("product_id") REFERENCES "product"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

}
