import {MigrationInterface, QueryRunner} from "typeorm";

export class addRating1605390399459 implements MigrationInterface {
    name = 'addRating1605390399459'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "venture" ADD "rating" integer`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "venture" DROP COLUMN "rating"`);
    }

}
