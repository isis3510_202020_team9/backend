import {MigrationInterface, QueryRunner} from "typeorm";

export class addLatitudeLongitude1602730163440 implements MigrationInterface {
    name = 'addLatitudeLongitude1602730163440'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "venture" ADD "latitude" double precision NOT NULL`);
        await queryRunner.query(`ALTER TABLE "venture" ADD "longitude" double precision NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "venture" DROP COLUMN "longitude"`);
        await queryRunner.query(`ALTER TABLE "venture" DROP COLUMN "latitude"`);
    }

}
