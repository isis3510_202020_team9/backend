import { sub } from "date-fns";
import { EntityManager, In } from "typeorm";
import { Order } from "../../entity/Order";
import { Product } from "../../entity/Product";

/**
 * User entity DAO
 */
export class ProductDAO {

    /**
     * DB Manager
     */
    private dbManager: EntityManager;

    /**
     * Initialize DAO with db manager
     * @param manager - DB Manager
     */
    constructor(manager: EntityManager) {
        this.dbManager = manager;
    }

    /**
     * Get trendy products
     */
    async getTrendyProducts(): Promise<Product[]> {
        // Orders of the last week
        const orders = await this.dbManager.createQueryBuilder(Order, 'or')
            .innerJoinAndSelect('or.products', 'pr')
            .select(['pr.product_id "productId"', 'count(*)'])
            .where('created_at > :date', { date: sub(new Date(), { weeks: 1 }) })
            .groupBy('pr.product_id')
            .orderBy('count', 'DESC')
            .take(20)
            .getRawMany();

        return await this.dbManager.getRepository(Product).findByIds(orders.map(order => order.productId), { relations: ['venture']});
    }

    async createProduct(product: Product): Promise<Product> {
        return await this.dbManager.save(product);
    }

    /**
     * Get a list of products
     * @param ids - Desired ids
     */
    async getProductsById(ids: string[]) : Promise<Product[]> {
        return await this.dbManager.find(Product, { where : { id: In(ids)}});
    }
}