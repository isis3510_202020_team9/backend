import { EntityManager } from "typeorm";
import { Order, OrderStatus } from "../../entity/Order";
import { OrderProducts } from "../../entity/OrderProducts";
import { Product } from "../../entity/Product";
import { generateError } from "../../utils/error/Error";
import { UserDAO } from "../user/UserDAO";
import { VentureDAO } from "../venture/VentureDAO";
import { ProductDAO } from "./ProductDAO";

interface OrderInfo {
    desiredDate?: Date;
    specifications?: string;
    productsInfo: { id: string; units: number }[];
    ventureId: string;
    address: string;
}

/**
 * Map products to order
 * @param products - Products
 * @param order - Order
 * @param manager - DB Manager
 */
export const mapProductsToOrder = async (products: { product: Product; units: number }[], order: Order, manager: EntityManager): Promise<number> => {
    let finalPrice = 0;
    const orderProducts: Promise<OrderProducts>[] = [];
    products.forEach(product => {
        let orderProduct = new OrderProducts();
        orderProduct.product = product.product;
        orderProduct.order = order;
        orderProduct.units = product.units;
        orderProducts.push(manager.save(orderProduct));
        finalPrice += (product.units * product.product.prize);
    });

    await Promise.all(orderProducts);
    return finalPrice;
};

/**
 * Buy products and create and order
 * @param userId - User ID
 * @param orderInformation - Order info 
 * @param manager - DB Manager
 */
export const buyProductModel = async (userId: string, orderInformation: OrderInfo, manager: EntityManager) => {
    // Get user by id
    const user = await new UserDAO(manager).getUserById(userId);

    let order = new Order();
    order.desiredDate = orderInformation.desiredDate;
    order.specifications = orderInformation.specifications;
    order.address = orderInformation.address;

    const productDAO = new ProductDAO(manager);

    const products = await productDAO.getProductsById(orderInformation.productsInfo.map(pr => pr.id));
    

    if (!products || products.length === 0) throw generateError('No hay productos asociados.');

    const ventureDAO = new VentureDAO(manager);

    const venture = await ventureDAO.getVentureById(orderInformation.ventureId);

    order.venture = venture;

    order = await manager.save(order);

    order.finalPrize = await mapProductsToOrder(products.map(product => {
        return {
            product,
            units: orderInformation.productsInfo.find(pr => pr.id === product.id)?.units
        }
    }), order, manager);

    order.user = user;
    return await manager.save(order);
};