import { sub } from "date-fns";
import { EntityManager } from "typeorm";
import { Order } from "../../entity/Order";
import { Product } from "../../entity/Product";
import { Venture } from "../../entity/Venture";

/**
 * Venture entity DAO
 */
export class VentureDAO {

    /**
     * DB Manager
     */
    private dbManager: EntityManager;

    /**
     * Initialize DAO with db manager
     * @param manager - DB Manager
     */
    constructor(manager: EntityManager) {
        this.dbManager = manager;
    }

    /**
     * Get all the Ventures at the database
     */
    async getVentures(): Promise<Venture[]> {
        return await this.dbManager.find(Venture);
    }

    /**
     * Insert a Venture in the database
     * @param Venture - Venture entity
     */
    async insertVenture(Venture: Venture): Promise<Venture> {
        return await this.dbManager.save(Venture);
    }

    /**
     * Get a Venture by his id
     * @param VentureId - Venture ID
     */
    async getVentureById(ventureId: string): Promise<Venture> {
        return await this.dbManager.findOne(Venture, ventureId);
    }

    /**
     * Get suggested ventures for a user
     * @param userId - User ID
     */
    async getVentureWithSuggestedUsers(userId: string): Promise<Venture[]> {
        const ventures = await this.dbManager.getRepository(Venture)
            .createQueryBuilder("vent")
            .leftJoin("vent.users", "user")
            .where('user.id = :id', { id: userId })
            .getMany();
        return ventures;
    }

    /**
     * Get trendy ventures
     */
    async getTrendyVentures(): Promise<Venture[]> {
        const orders = await this.dbManager.createQueryBuilder(Order, 'or')
            .innerJoinAndSelect('or.venture', 've')
            .select(['ve.id, count(*)'])
            .where('created_at > :date', { date: sub(new Date(), { weeks: 1 }) })
            .groupBy('ve.id')
            .orderBy('count', 'DESC')
            .take(20)
            .getRawMany();

        return await this.dbManager.getRepository(Venture).findByIds(orders.map(order => order.id));
    }

    /**
     * Get orders from a venture
     * @param ventureId - Venture ID
     */
    async getOrders(ventureId: string) : Promise <Order[]> {
        return await this.dbManager.find(Order, { where: { venture: { id: ventureId }}, relations: ['products', 'products.product']});
    }

    /**
     * Get order from a venture
     * @param orderId - Order id
     * @param ventureId - Venture id
     */
    async getOrderFromVenture(orderId: string, ventureId: string) {
        return await this.dbManager.findOne(Order, { where: { venture: { id: ventureId }, id: orderId }});
    }

    /**
     * Update order
     * @param orderId - Order id 
     */
    async updateOrder(orderId: string, order: Partial<Order>) {
        return await this.dbManager.update(Order, orderId,  order);
    }

    /**
     * Get products of a venture
     * @param ventureId - Venture ID
     */
    async getProducts(ventureId: string) : Promise <Product[]> {
        return await this.dbManager.find(Product, { where : { venture: { id: ventureId }}});
    }
}