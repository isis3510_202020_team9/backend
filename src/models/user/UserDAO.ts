import { EntityManager } from "typeorm";
import { User } from "../../entity/User";
import { GenericRepository } from "../../utils/db/Database";

/**
 * User entity DAO
 */
export class UserDAO {

    /**
     * DB Manager
     */
    private dbManager: EntityManager;

    /**
     * Initialize DAO with db manager
     * @param manager - DB Manager
     */
    constructor(manager: EntityManager) {
        this.dbManager = manager;
    }

    /**
     * Get all the users at the database
     */
    async getUsers(): Promise<User[]> {
        return await this.dbManager.find(User);
    }

    /**
     * Insert a user in the database
     * @param user - User entity
     */
    async insertUser(user: User) : Promise<User> {
        return await this.dbManager.save(user);
    }

    /**
     * Update a user in the database
     * @param user - User entity
     */
    async updateUser(userId:string, user: Partial<User>) : Promise<User> {
        await this.dbManager.update(User,userId,user);
        return this.getUserById(userId);
    }

    /**
     * Get a user by his id
     * @param userId - User ID
     */
    async getUserById(userId: string) : Promise<User> {
        return await this.dbManager.findOne(User, userId);
    }
    /**
     * Get a user by his email
     * @param email - User email
     */
    async getUserByEmail(email: string) : Promise<User> {
        return await this.dbManager.findOne(User, { where: { email }});
    }

    async getUserWithSuggested(userId:string): Promise<User> {
        const userRepository = new GenericRepository<User>(this.dbManager.getRepository(User));
        const relations = await userRepository.findWithRelations(['suggestedProducts', 'suggestedVentures'], { where: { id: userId}});    
        return relations[0];  
    }

}