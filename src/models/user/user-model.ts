import { EntityManager } from "typeorm";
import { questions } from "../../const/questions";
import { Product } from "../../entity/Product";
import { User } from "../../entity/User";
import { UserPreferredCategory } from "../../entity/UserPreferredCategories";
import { Venture } from "../../entity/Venture";
import { Category } from "../../entity/VentureCategory";
import { generateToken, hashPassword, verifyPassword } from "../../utils/auth/jwt";
import { generateError } from "../../utils/error/Error";
import { UserDAO } from "./UserDAO";

/**
 * Log in a user in the app
 * @param email - Email entered
 * @param password - Password entered
 * @param manager - DB Manager
 */
export const logInModel = async (email: string, password: string, manager: EntityManager) => {
    const userDao = new UserDAO(manager);

    // Find a user with that email
    let user = await userDao.getUserByEmail(email);

    if (!user) throw generateError('No existe una cuenta con este correo.');

    // Check if password is correct
    const validationResult = await verifyPassword(password, user.password);

    if (! await validationResult) throw generateError(`Password incorrect for the user with email ${email}`);

    return { token: generateToken(user.id), hasProfiling: user.hasProfiling, role: user.role };
};

/**
 * Sign up a user in the app
 * @param email - Desired email
 * @param password - Desired password
 * @param manager - DB Manager
 */
export const signUpModel = async (email: string, password: string, manager: EntityManager) => {
    const userDao = new UserDAO(manager);

    let user = await userDao.getUserByEmail(email);

    if (user) throw generateError(`Ya existe un usario con este correo electrónico`);

    user = new User();

    // verify if email is valid
    if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email)) {
        // verify if password is valid
        if (/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/.test(password)) {
            const pss = await hashPassword(password);
            user.email = email;
            user.password = pss;
            user = await userDao.insertUser(user);
        }
        else throw generateError(`The password is not valid`);
    }
    else throw generateError(`The email ( ${email} ) is not valid`);
    return { token: generateToken(user.id), hasProfiling: false, role: user.role };
};

/**
 * Assign weights to each category
 * @param current 
 * @param obj 
 */
const assignWeights = (current: { category: Category, count: number }[], addition: { category: Category, weight: number }[]): { category: Category, count: number }[] => {
    current.forEach(categoryCounter => categoryCounter.count += addition.find(c => c.category === categoryCounter.category).weight);
    return current;
}

/**
 * Profiling model
 * @param responses - Responses from the user 
 * @param userId - User ID
 * @param manager - DB Manager
 */
export const profilingModel = async (responses: any, userId: string, manager: EntityManager) => {
    const userDao = new UserDAO(manager);

    // Get user by id
    const user = await userDao.getUserWithSuggested(userId);

    console.log('User id', user.id);
    if (user.suggestedProducts && user.suggestedProducts?.length > 0) throw generateError('The user has already profiling');

    let finalCount: { category: Category, count: number }[] = Object.values(Category).map(ct => { return { category: Category[ct], count: 0 } });

    for (const response of responses) {
        finalCount = assignWeights(finalCount, questions[(response.question - 1)].answers.find(answer => answer.id === response.answer).weights);
    }

    finalCount.sort((a, b) => b.count - a.count);

    const selectedCountCategories = finalCount.slice(0, 2);

    const selectedCategories = selectedCountCategories.map(cat => cat.category);

    // User preferred categories
    const preferredCategories = selectedCategories.map(cat => {
        const preferredCategory = new UserPreferredCategory();
        preferredCategory.category = cat;
        preferredCategory.user_id = user.id;
        return preferredCategory;
    })

    const promises = preferredCategories.map(category => manager.save(category));
    const ventures = await manager.createQueryBuilder(Venture, 'vt')
        .leftJoinAndSelect('vt.categories', 'ct')
        .where('ct.name IN (:...names)', { names: selectedCategories })
        .getMany();

    const products = await manager.createQueryBuilder(Product, 'pr')
        .where('pr.product_category IN (:...categories)', { categories: selectedCategories })
        .getMany();


    // Save suggested ventures and products
    user.suggestedVentures.push(...ventures);
    user.suggestedProducts.push(...products);

    user.hasProfiling = true;
    await manager.save(User, user);
    await Promise.all(promises);
    return { message: 'Profiling finished' };
}