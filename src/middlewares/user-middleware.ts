import { EntityManager } from "typeorm";
import { Order } from "../entity/Order";
import { logInModel, profilingModel, signUpModel } from "../models/user/user-model";

/**
 * Log in handler
 * @param event
 * @param manager- DB Manager
 */
export const logIn = async (event, manager: EntityManager) => {
  const { email, password } = event.body;
  return await logInModel(email, password, manager);
};

/**
 * Sign up handler
 * @param event 
 * @param manager - DB Manager
 */
export const signUp = async(event, manager: EntityManager) => {
    const { email, password } = event.body;
    return await signUpModel(email, password, manager);
}

/**
 * User profiling
 * @param event 
 * @param manager - DB Manager
 */
export const profileUser = async (event,userId: string, manager: EntityManager) => {
  const { responses } = event.body;
  return await profilingModel(responses, userId, manager );
};

/**
 * Get an oder by its id and verify if belongs to the user
 * @param event 
 * @param userId - User ID
 * @param manager - DB Manager
 */
export const getOrder = async (event, manager: EntityManager) => {
  const { orderId } = event.params;
  return await manager.findOne(Order, { where: { id: orderId}, relations: ['products', 'products.product']});
};