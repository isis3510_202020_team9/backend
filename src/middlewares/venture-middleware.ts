import { Entity, EntityManager, In } from "typeorm";
import { Product } from "../entity/Product";
import { Venture, VentureReduced } from "../entity/Venture";
import { forwardGeocoding, getDistance } from "../utils/geolocation";
import _ from 'lodash';
import { VentureDAO } from "../models/venture/VentureDAO";
import { generateError } from "../utils/error/Error";
import { UserDAO } from "../models/user/UserDAO";
import { ProductDAO } from "../models/product/ProductDAO";
import { UserRole } from "../entity/User";
import { VentureCategory } from "../entity/VentureCategory";

/**
 * Get suggested ventures
 * @param event 
 * @param userId 
 * @param manager 
 */
export const getSuggestedVentures = async (event, userId, manager: EntityManager) => {
    const ventureDAO = new VentureDAO(manager);
    const suggestedVentures = await ventureDAO.getVentureWithSuggestedUsers(userId);
    if (!suggestedVentures) return [];
    return suggestedVentures;
}

/**
 * Get near ventures
 * @param event 
 * @param manager - DB Manager
 */
export const getNearVentures = async (event, userId: string, manager: EntityManager) => {
    const ventureDAO = new VentureDAO(manager);
    const userDAO = new UserDAO(manager);

    const { latitude, longitude } = event.query;

    if (!latitude || !longitude ) throw generateError('You must specify the latitude and longitude of the user');

    const updatePromise = userDAO.updateUser(userId, { latitude, longitude });

    const nearVentures: VentureReduced[] = [];
    const nearProducts: Product[] = [];
    const ventures = await ventureDAO.getVentures();

    for (const venture of ventures) {
        // Calculate distance between coordinates.
        const distance = getDistance({ latitude, longitude }, { latitude: venture.latitude, longitude: venture.longitude });

        if (distance < 3) {
            let ventureReduced = new VentureReduced();
            // Reduce venture to relevant fields
            _.assign(ventureReduced, _.pick(venture, _.keys(ventureReduced)));
            let products = venture.products;
            delete venture.products;
            products = products.map(pr => {return { ...pr , venture }});
            nearVentures.push(ventureReduced);
            nearProducts.push(...products);
        }
    }
    await updatePromise;
    return { nearVentures, nearProducts };
};

/**
 * Get trendy ventures
 * @param event - HTTP event
 * @param manager - DB Manager
 */
export const getTrendyVentures = async (manager: EntityManager) => {
    const venture = new VentureDAO(manager);
    return await venture.getTrendyVentures();
};

/**
 * Get venture orders
 * @param userId - User ID
 * @param manager - DB Manager
 */
export const getVentureOrders = async (userId: string, manager: EntityManager) => {
    const venture = new VentureDAO(manager);
    return await venture.getOrders(userId);
}

/**
 * Create a product
 * @param product - Product info
 * @param manager - DB Manager
 */
export const createProduct = async (product: Partial<Product> , userId: string, manager: EntityManager) => {
    const productDAO = new ProductDAO(manager);
    const newProduct = new Product();

    newProduct.photo = 'http://placeimg.com/375/320';

    Object.assign(newProduct, product);

    newProduct.venture = await manager.findOne(Venture, userId);

    return await productDAO.createProduct(newProduct);
};
/**
 * Convert into venture
 * @param partialVenture - Partial venture info
 * @param userId - User ID
 * @param manager - DB Manager
 */
export const convertIntoVenture = async (event, userId: string, manager: EntityManager) => {
    const { categories } = event.body;

    delete event.body?.categories;
    const userDAO = new UserDAO(manager);

    const user = await userDAO.getUserById(userId);

    await userDAO.updateUser(user.id, { role : UserRole.VENTURE });
    
    if (!user) throw generateError('The user doesnt exist');

    // Venture
    const venture = new Venture();
    venture.id = user.id;
    
    const {latitude, longitude} = await forwardGeocoding(event.body.address);
    venture.latitude = latitude || 4.732537;
    venture.longitude = longitude || -74.034284;
    venture.categories = await manager.find(VentureCategory, { where: {name: In(categories)}});
    venture.photo = 'http://placeimg.com/375/320';
    Object.assign(venture, event.body);

    const ventureDAO = new VentureDAO(manager);

    return await ventureDAO.insertVenture(venture);
}

/**
 * Get products from a venture
 * @param event 
 * @param manager - DB Manager
 */
export const getProducts = async (event, manager: EntityManager) => {

    const { ventureId } = event.params;

    const ventureDAO = new VentureDAO(manager);

    return await ventureDAO.getProducts(ventureId);
}

/**
 * Update order status
 * @param event 
 * @param ventureId - Venture ID
 * @param manager - DB Manager
 */
export const updateOrderStatus = async(event, ventureId, manager: EntityManager) => {

    const { status } = event.body;
    const { orderId } = event.params;

    const ventureDAO = new VentureDAO(manager);

    const order = await ventureDAO.getOrderFromVenture(orderId,ventureId);

    if (!order) throw generateError(`The order with id ${orderId} doesnt belong to this site`);
    order.status = status;
    await ventureDAO.updateOrder(orderId, { status });

    return order;
};

/**
 * Get venture by its id
 * @param event 
 * @param ventureId - Venture ID 
 * @param manager - DB Manager
 */
export const getVenture = async (event, ventureId:string, manager: EntityManager) => {
    return await manager.findOne(Venture, ventureId);
}