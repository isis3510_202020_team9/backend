import { EntityManager } from "typeorm";
import { Order } from "../entity/Order";
import { Product } from "../entity/Product";
import { buyProductModel } from "../models/product/product-model";
import { ProductDAO } from "../models/product/ProductDAO";

/**
 * Get suggested products
 * @param event - HTTP event
 * @param userId - User ID
 * @param manager - DB Manager
 */
export const getSuggestedProducts = async (event, userId, manager: EntityManager) => {
    const { page } = event.query;
    
    const suggestedProducts = await manager.getRepository(Product)
        .createQueryBuilder("prd")
        .leftJoinAndSelect("prd.venture", "v")
        .leftJoin("prd.users", "user")
        .where('user.id = :id', { id: userId })
        .orderBy('prd.name', 'DESC')
        .getMany();
    
    if (!suggestedProducts) return [];
    return suggestedProducts.slice((page - 1 ) * 20, 20 * page);
}

/**
 * Get trendy products
 * @param event - HTTP event
 * @param userId - User ID
 * @param manager - DB Manager
 */
export const getTrendyProducts = async (event, manager: EntityManager) => {
    const product = new ProductDAO(manager);
    return await product.getTrendyProducts();
};

/**
 * Buy products from a venture
 * @param event 
 * @param userId - User id
 * @param manager - DB Manger
 */
export const buyProducts = async (event, userId: string, manager: EntityManager) => {
    return await buyProductModel(userId,event.body,manager);
};